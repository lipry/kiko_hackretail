package kikoapp.hack.com.kikoapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
public class DetailsFragment extends Fragment {
    View rootView;
    ImageView ivFotoProfilo, ivFoto;
    TextView tvDesc, tvUsername, tvHashtags, tvProdotti;
    ImageButton imBtnFollow;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_details, container, false);
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvDesc = (TextView) rootView.findViewById(R.id.tvDesc);
        tvUsername = (TextView) rootView.findViewById(R.id.tvUsername);
        tvHashtags = (TextView) rootView.findViewById(R.id.tvHashtag);
        tvProdotti = (TextView) rootView.findViewById(R.id.tvProdotti);
        ivFotoProfilo = (ImageView) rootView.findViewById(R.id.ivFotoProfilo);
        ivFoto = (ImageView) rootView.findViewById(R.id.ivFoto);
        final Bundle dati = this.getArguments();
        if(dati != null) {
            tvDesc.setText(dati.getString("desc"));
            tvUsername.setText(dati.getString("username"));
            tvHashtags.setText(dati.getString("hashtag"));
            ivFotoProfilo.setImageDrawable(getResources().getDrawable((dati.getInt("imFotoProfilo"))));
            ivFoto.setImageDrawable(getResources().getDrawable((dati.getInt("foto"))));

            ivFotoProfilo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostraProfilo(tvUsername.getText().toString(), dati.getInt("imFotoProfilo"));
                }
            });
            tvUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostraProfilo(tvUsername.getText().toString(), dati.getInt("imFotoProfilo"));
                }
            });
        }
    }
    public void mostraProfilo(String username, int foto){
        Bundle params = new Bundle();
        params.putString("username",username);
        params.putInt("imFotoProfilo", foto);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new FragmentProfilo();
        fragment.setArguments(params);
        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit();
    }
}
