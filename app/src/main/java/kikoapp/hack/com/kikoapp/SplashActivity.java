package kikoapp.hack.com.kikoapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

public class SplashActivity extends Activity {

        public SplashActivity(){}
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            Thread timer = new Thread(){
                public void run(){
                    try{
                        sleep(1000);
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            };
            timer.start();
        }
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
 }