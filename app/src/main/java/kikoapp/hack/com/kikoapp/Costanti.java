package kikoapp.hack.com.kikoapp;

/**
 * Created by fabiolipreri on 16/05/15.
 */
public class Costanti {
    public static String[] hashtags = {
        " #clean #beauty #everyday #white #classy #work #university #pink",
        "#mystery #magnetic #secret #classic #eyesonme #sexy",
        "80s #party #nightout #joking #pop #art #nails",
        "#ready #for #workout #clean #nosweat #easy"
    };
    public static String[] desc = {
            "Sweet",
            "Femme Fatale",
            "Glamour",
            "Sport"

    };
    public static int[] immaginiProfilo = {
            R.drawable.sweet_1,
            R.drawable.femmefatale_1,
            R.drawable.glam_1,
            R.drawable.sport_1
    };
    public static int[] all = {
            R.drawable.sweet_1,
            R.drawable.femmefatale_1,
            R.drawable.glam_1,
            R.drawable.sport_1,
            R.drawable.sweet_1,
            R.drawable.sweet_2,
            R.drawable.sweet_3,
            R.drawable.sweet_4,
            R.drawable.glam_1,
            R.drawable.glam_2,
            R.drawable.glam_3,
            R.drawable.glam_4,
            R.drawable.femmefatale_1,
            R.drawable.femmefatale_2,
            R.drawable.sweet_1,
            R.drawable.femmefatale_1,
            R.drawable.glam_1,
            R.drawable.sport_1
    };
    public static int[] imgSweet = {
            R.drawable.sweet_1,
            R.drawable.sweet_2,
            R.drawable.sweet_3,
            R.drawable.sweet_4
    };
    public static int[] imgGlamour = {
            R.drawable.glam_1,
            R.drawable.glam_2,
            R.drawable.glam_3,
            R.drawable.glam_4

    };
    public static int[] imgFemmeFatale = {
            R.drawable.femmefatale_1,
            R.drawable.femmefatale_2
    };
    public static int[] imgSport = {
            R.drawable.sport_1,
            R.drawable.sport_2
    };
    public static String[] username= {
        "Anna",
        "Sara",
        "Maria",
        "Federica"
    };

    public static String[] productList = {
            "Product1",
            "Product2",
            "Product3",
            "Product4"

    };
}
