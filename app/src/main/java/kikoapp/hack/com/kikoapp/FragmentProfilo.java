package kikoapp.hack.com.kikoapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentProfilo extends Fragment {
    View rootView;
    GridView grid;
    int[] imageId = {
            R.drawable.sport_1,
            R.drawable.sport_2,
            R.drawable.femmefatale_1,
            R.drawable.sweet_2,
            R.drawable.glam_1,
            R.drawable.sweet_1,
            R.drawable.sweet_3,
            R.drawable.femmefatale_2
    };
    TextView tvUsername;
    ImageView ivFotoProfilo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_profilo, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvUsername = (TextView) rootView.findViewById(R.id.tvUsername);
        ivFotoProfilo = (ImageView) rootView.findViewById(R.id.ivFotoProfilo);
        Bundle dati = this.getArguments();
        tvUsername.setText(dati.getString("username"));
        ivFotoProfilo.setImageDrawable(getResources().getDrawable((dati.getInt("imFotoProfilo"))));


        CustomGrid adapter = new CustomGrid(getActivity(), imageId);
        grid=(GridView)rootView.findViewById(R.id.gvFeed);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Bundle params = new Bundle();
                params.putInt("imFotoProfilo", Costanti.immaginiProfilo[0]);
                params.putString("username", Costanti.username[0]);
                params.putString("desc",Costanti.desc[0]);
                params.putString("hashtag",Costanti.hashtags[0]);
                params.putString("productList", Costanti.productList[0]);
                params.putInt("foto", imageId[position]);
                MainActivity.setTipo(0);
                Fragment fragment = new DetailsFragment();
                fragment.setArguments(params);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
            }
        });
    }
}
