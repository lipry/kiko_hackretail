package kikoapp.hack.com.kikoapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

public class FeedFragment extends Fragment implements View.OnClickListener{
    View rootView;
    GridView grid;
    int[] images;
    ImageView ivSweet, ivFemmeFatale, ivGlamour, ivSport, ivAll;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.feed_fragment, container, false);
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ivAll= (ImageView) rootView.findViewById(R.id.ivAll);
        ivSweet = (ImageView) rootView.findViewById(R.id.ivSweet);
        ivFemmeFatale = (ImageView) rootView.findViewById(R.id.ivFemmeFatale);
        ivGlamour = (ImageView) rootView.findViewById(R.id.ivGlamour);
        ivSport = (ImageView) rootView.findViewById(R.id.ivSport);
        ivAll.setOnClickListener(this);
        ivSweet.setOnClickListener(this);
        ivFemmeFatale.setOnClickListener(this);
        ivGlamour.setOnClickListener(this);
        ivSport.setOnClickListener(this);

        images = Costanti.all;
        CustomGrid adapter = new CustomGrid(getActivity(), images);
        grid=(GridView)rootView.findViewById(R.id.gvFeed);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Bundle params = new Bundle();
                params.putInt("imFotoProfilo", Costanti.immaginiProfilo[0]);
                params.putString("username", Costanti.username[0]);
                params.putString("desc",Costanti.desc[0]);
                params.putString("hashtag",Costanti.hashtags[0]);
                params.putString("productList", Costanti.productList[0]);
                params.putInt("foto", images[position]);
                Fragment fragment = new DetailsFragment();
                fragment.setArguments(params);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.ivAll:
                images = Costanti.all;
                break;
            case R.id.ivSweet:
                images = Costanti.imgSweet;
                break;
            case R.id.ivFemmeFatale:
                images = Costanti.imgFemmeFatale;
                break;
            case R.id.ivGlamour:
                images = Costanti.imgGlamour;
                break;
            case R.id.ivSport:
                images = Costanti.imgSport;
                break;
        }
        filtra();
    }
    public void filtra(){
        CustomGrid adapter = new CustomGrid(getActivity(), images);
        grid=(GridView)rootView.findViewById(R.id.gvFeed);
        grid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
